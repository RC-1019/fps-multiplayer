using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;

public class RoomButton : MonoBehaviour
{
    #region Variables

    public RoomInfo roomInfo;
    [SerializeField] private TMP_Text buttonText;

    #endregion

    #region Unity Functions


    #endregion

    #region Custom Functions

    public void SetButtonDetails(RoomInfo inputInfo)
    {
        roomInfo = inputInfo;
        buttonText.text = roomInfo.Name;
    }

    public void JoinRoom()
    {
        Launcher.Instance.JoinRoom(roomInfo);
    }

    #endregion
}
