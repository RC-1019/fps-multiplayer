using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    #region Variables

    public Gun_Data data;
    public Transform muzzlePoint;
    public AudioSource disparo;
    public AudioSource recarga;
    public AudioClip cambioArma;

    #endregion

    #region Unity Functions

    private void Awake()
    {
        data.actualAmmo = data.maxAmmoCount;
    }

    #endregion
}
