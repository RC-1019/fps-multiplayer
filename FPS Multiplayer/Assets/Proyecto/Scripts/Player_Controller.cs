using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour
{
    #region Variables

    [Header("Elementos c�mara")]
    [SerializeField] internal Camera cam;
    [SerializeField] private Transform pointOfView;
    public Transform gunPoint;
    private float horizontalRotationStore;
    private float verticalRotationStore;
    private Vector2 mouseInput;


    [Header("Elementos de movimiento")]
    private Vector3 direction;
    [SerializeField] private float walkSpeed;
    [SerializeField] private float runSpeed;
    private Vector3 movement;
    [SerializeField] private CharacterController controller;
    private float actualSpeed;
    [SerializeField] private float jumpForce;
    [SerializeField] private float gravityMod;

    [Header("Deteccion del suelo")]
    [SerializeField] private bool isGrounded;
    [SerializeField] private float radio;
    [SerializeField] private float distance;
    [SerializeField] private Vector3 offset;
    [SerializeField] private LayerMask lm;

    [Header("Elementos del arma")]
    public Transform recoil;

    #endregion

    #region Functions

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        cam = Camera.main;
    }

    private void Update()
    {
        Rotation();
        Movement();

    }

    private void LateUpdate()
    {
        cam.transform.position = recoil.position;
        cam.transform.rotation = recoil.rotation;

        gunPoint.transform.position = recoil.position;
        gunPoint.transform.rotation = recoil.rotation;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position + offset, radio);
        if (Physics.SphereCast(transform.position + offset, radio, Vector3.down, out RaycastHit hit, distance, lm))
        {
            Gizmos.color = Color.green;
            Vector3 endPoint = ((transform.position + offset) + (Vector3.down * distance));
            Gizmos.DrawWireSphere(endPoint,radio);
            Gizmos.DrawLine(transform.position + offset, endPoint);

            Gizmos.DrawSphere(hit.point, 0.1f);
        }
        else
        {
            Gizmos.color = Color.red;
            Vector3 endPoint = ((transform.position + offset) + (Vector3.down * distance));
            Gizmos.DrawWireSphere(endPoint, radio);
            Gizmos.DrawLine(transform.position + offset, endPoint);
        }
    }

    #endregion

    #region Custom Functions

    private void Rotation()
    {
        mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        horizontalRotationStore += mouseInput.x;
        verticalRotationStore -= mouseInput.y;

        verticalRotationStore = Mathf.Clamp(verticalRotationStore, -60f, 60f);

        transform.rotation = Quaternion.Euler(0f, horizontalRotationStore, 0f);
        pointOfView.transform.localRotation = Quaternion.Euler(verticalRotationStore, transform.rotation.y, 0f);

    }

    private void Movement()
    {
        direction = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));

        float velY = movement.y;
        movement = ((transform.forward * direction.z) + (transform.right * direction.x)).normalized;
        movement.y = velY;


        if (Input.GetButtonDown("Run"))
        {
            actualSpeed = runSpeed;
        }
        else
        {
            actualSpeed = walkSpeed;
        }

        if (IsGrounded())
        {
            movement.y = 0;
        }

        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            movement.y = jumpForce * Time.deltaTime;
        }

        movement.y += Physics.gravity.y * Time.deltaTime * gravityMod;
        controller.Move(movement * (actualSpeed * Time.deltaTime));
    }

    private bool IsGrounded()
    {
        isGrounded = false;

        if (Physics.SphereCast(transform.position + offset,radio,Vector3.down,out RaycastHit hit,distance,lm))
        {
            isGrounded = true;
        }

        return isGrounded;
    }

    #endregion
}
