using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Gun_Controller : MonoBehaviour
{
    #region Variables

    [Header("Elementos del arma")]
    public Gun[] guns;
    public Gun actualGun;
    public int indexGun = 0;
    public int maxGuns = 3;
    public GameObject prefBulletHole;
    public float lastReload;
    public bool reloading;
    bool isChanging;
    float lastChangeTime;
    float changeTime;

    float lastShotTime = 0;
    Vector3 currentRotation;
    Vector3 targetRotation;
    public float returnSpeed;
    public float snappines;
    public VisualEffect vfxMuzzle;


    [Header("Elementos del jugador")]
    public Player_Controller playerController;

    #endregion

    #region Unity Functions

    private void Update()
    {
        if (actualGun != null)
        {
            if (lastShotTime <= 0 && !reloading)
            {
                if (!actualGun.data.automatic)
                {
                    if (Input.GetButtonDown("Fire1"))
                    {
                        if (actualGun.data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }
                }
                else
                {
                    if (Input.GetButton("Fire1"))
                    {
                        if (actualGun.data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }
                }
            }
            if (Input.GetButtonDown("Reload") && !reloading)
            {
                if (actualGun.data.actualAmmo < actualGun.data.maxAmmoCount)
                {
                    lastReload = 0;
                    actualGun.recarga.Play();
                    reloading = true;
                }
            }
        }
        else
        {

        }

        if (lastShotTime >= 0)
        {
            lastShotTime -= Time.deltaTime;
        }

        if (reloading)
        {
            lastReload += Time.deltaTime;
            if (lastReload >= actualGun.data.reloadTime)
            {
                reloading = false;
                Reload();
            }
        }


        targetRotation = Vector3.Lerp(targetRotation, Vector3.zero, returnSpeed * Time.deltaTime);
        currentRotation = Vector3.Slerp(currentRotation, targetRotation, snappines * Time.deltaTime);
        playerController.recoil.localRotation = Quaternion.Euler(currentRotation);
        ChangeGun(indexGun);

        if (Input.GetButtonDown("Gun1") && !reloading)
        {
            if (indexGun != 0)
            {
                indexGun = 0;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChanging = true;
            }
        }

        if (Input.GetButtonDown("Gun2") && !reloading)
        {
            if (indexGun != 1)
            {
                indexGun = 1;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChanging = true;
            }
        }

        if (Input.GetButtonDown("Gun3") && !reloading)
        {
            if (indexGun != 2)
            {
                indexGun = 2;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChanging = true;
            }
        }

        if (isChanging)
        {
            lastChangeTime += Time.deltaTime;
            if (lastChangeTime >= changeTime)
            {
                isChanging = false;
                ChangeGun(indexGun);
                AudioSource.PlayClipAtPoint(actualGun.cambioArma, gameObject.transform.position);
            }
        }
    }

    #endregion


    #region Custom Functions

    private void Shoot()
    {
        if (Physics.Raycast(playerController.cam.transform.position,playerController.cam.transform.forward,out RaycastHit hit,actualGun.data.range))
        {
            if (hit.transform != null)
            {
                Debug.Log($"We're shooting at: {hit.transform.name}");
                GameObject go = Instantiate(prefBulletHole, hit.point + hit.normal * 0.01f, Quaternion.LookRotation(hit.normal, Vector3.up));
                Destroy(go, 2.5f);
            }
        }
        actualGun.data.actualAmmo--;
        lastShotTime = actualGun.data.fireRate;
        AddRecoil();
        actualGun.disparo.Play();
        GameObject vfx = VisualEffect.Instantiate(vfxMuzzle, actualGun.muzzlePoint.position, actualGun.muzzlePoint.rotation).gameObject;
        vfx.transform.parent = actualGun.muzzlePoint;
        Destroy(vfx, 1.5f);
    }

    void AddRecoil()
    {
        targetRotation += new Vector3(actualGun.data.recoil.x, Random.Range(-actualGun.data.recoil.y, actualGun.data.recoil.y), 0);
    }

    void Reload()
    {
        actualGun.data.actualAmmo = actualGun.data.maxAmmoCount;
    }

    void ChangeGun(int index)
    {
        if (guns[index] != null)
        {
            actualGun = null;
            actualGun = guns[index];
            actualGun.gameObject.SetActive(true);
        }
    }

    void Interaction(Transform floor_gun, int index)
    {
        if (actualGun != null)
        {
            ReleasedGun(actualGun, indexGun);
        }
        Rigidbody rb = floor_gun.GetComponent<Rigidbody>();
        rb.isKinematic = true;
        floor_gun.transform.parent = playerController.gunPoint;
        actualGun = floor_gun.transform.gameObject.GetComponent<Gun>();
        floor_gun.transform.localPosition = actualGun.data.offset;
        floor_gun.transform.localRotation = Quaternion.identity;
        guns[index] = actualGun;

    }

    void ReleasedGun(Gun floor_gun, int index)
    {
        Rigidbody rb = floor_gun.GetComponent<Rigidbody>();
        actualGun.transform.parent = null;
        guns[index] = null;
        actualGun = null;
        rb.isKinematic = false;
        rb.AddForce(floor_gun.transform.up * 2, ForceMode.Impulse);
        rb.AddTorque(floor_gun.transform.right * 2, ForceMode.Impulse);
    }

    #endregion
}
